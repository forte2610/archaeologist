﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design.Serialization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using _1412093.Maze;

namespace _1412093.Sprite {
    class Scorpion : NPC {
        public Scorpion(Sprite2D sprite, int x, int y, int hp, int dam) : base(sprite, x, y, hp, dam)
        {
            IList<Texture2D> textures = new List<Texture2D>();
            textures = ResourceManager.Instance.LoadSingleTexture("textures/npcs/scorpion");
            this.Sprite = new Sprite2D(textures, Global.Instance.CellSize * this.CellY, Global.Instance.CellSize * this.CellX, 0.2f);
        }

        public bool isMouseInside(Vector2 mousePos) {
            if (mousePos.X >= this.Sprite.Left && mousePos.X <= this.Sprite.Left + this.Sprite.Width &&
                mousePos.Y >= this.Sprite.Top && mousePos.Y <= this.Sprite.Top + this.Sprite.Height) {
                return true;
            }
            return false;
        }

        public void Select(bool isSelected, Vector2 mousePos) {
            if (isSelected && isMouseInside(mousePos)) this.Sprite.State = GameUIControl.STATE_PRESSED;
        }

        public override bool Detect(Cell[,] mazeState, Vector2 playerPos) {
            // Up - Left
            for (int i = 0; i < 4 && this.CellX - i >= 0 && this.CellY - i >= 0; i++) {
                if (mazeState[this.CellX - i, this.CellY - i].State == 1) break;
                if (mazeState[this.CellX - i, this.CellY - i].X == playerPos.X && mazeState[this.CellX - i, this.CellY - i].Y == playerPos.Y)
                    return true;
            }
            // Down - Left
            for (int i = 0; i < 4 && this.CellX - i >= 0 && this.CellY + i <= MazeGenerator.Instance.Rows - 1; i++) {
                if (mazeState[this.CellX - i, this.CellY + i].State == 1) break;
                if (mazeState[this.CellX - i, this.CellY + i].X == playerPos.X && mazeState[this.CellX - i, this.CellY + i].Y == playerPos.Y)
                    return true;
            }
            // Up - Right
                for (int i = 0; i < 4 && this.CellX + i <= MazeGenerator.Instance.Columns - 1 && this.CellY - i >= 0; i++) {
                if (mazeState[this.CellX + i, this.CellY - i].State == 1) break;
                if (mazeState[this.CellX + i, this.CellY - i].X == playerPos.X && mazeState[this.CellX + i, this.CellY + i].Y == playerPos.Y)
                    return true;
            }
            // Down - Right
            for (int i = 0; i < 4 && this.CellX + i <= MazeGenerator.Instance.Columns - 1 && this.CellY + i <= MazeGenerator.Instance.Rows - 1; i++) {
                if (mazeState[this.CellX + i, this.CellY + i].State == 1) break;
                if (mazeState[this.CellX + i, this.CellY + i].X == playerPos.X && mazeState[this.CellX + i, this.CellY + i].Y == playerPos.Y)
                    return true;
            }
            return false;
        }

        public override void Update(GameTime gameTime) {
            MouseState ms = Mouse.GetState();
            Vector2 mousePos = new Vector2(ms.X, ms.Y);
            if (ms.LeftButton == ButtonState.Pressed) {
                Select(true, mousePos);
            }
            else Select(false, mousePos);
            base.Update(gameTime);
            if (!isMouseInside(mousePos)) this.Sprite.State = GameUIControl.STATE_NORMAL;
        }
    }
}
