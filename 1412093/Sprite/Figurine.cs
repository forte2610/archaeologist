﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using _1412093.Maze;

namespace _1412093.Sprite {
    class Figurine : Treasure {
        private int _quality;

        public Figurine(Sprite2D sprite, int x, int y) : base(sprite, x, y)
        {
            this.Sprite = sprite;
            this.CellX = x;
            this.CellY = y;
            this.Quality = MazeGenerator.Instance.RNG.Next(10, 101);
            this.Age = MazeGenerator.Instance.RNG.Next(999, 99999);
            this.Weight = (float)(0.5f + MazeGenerator.Instance.RNG.NextDouble() * 15f);
            byte figSkin = (byte)MazeGenerator.Instance.RNG.Next(1, 5);
            IList<Texture2D> textures = ResourceManager.Instance.LoadSingleTexture("textures/items/fig" + figSkin.ToString());
            this.Sprite = new Sprite2D(textures, Global.Instance.CellSize * this.CellY, Global.Instance.CellSize * this.CellX, 0.2f);
        }

        public int Quality {
            get { return _quality; }
            set { _quality = value; }
        }
    }
}
