﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace _1412093 {
    class MouseEvent : GameInvisibleEntity {
        public delegate void OnLMouseDownHandler(object sender, MouseEventArgs e);
        public delegate void OnLMouseUpHandler(object sender, MouseEventArgs e);
        public event OnLMouseDownHandler OnLMouseDown;
        public event OnLMouseUpHandler OnLMouseUp;

        MouseState _previous;
        MouseState _current;

        public bool IsLMouseDown() {
            if (_current.LeftButton == ButtonState.Pressed)
                return true;
            return false;
        }

        public Vector2 GetPos() {
            return new Vector2(_current.X, _current.Y);
        }

        public Vector2 GetDiff() {
            return new Vector2(_current.X - _previous.X, _current.Y - _previous.Y);
        }

        public bool IsLMouseUp() {
            return (_current.LeftButton == ButtonState.Released) ? true : false;
        }

        public override void Update(GameTime gameTime) {
            base.Update(gameTime);
            if (_previous == null)
                _previous = _current = Mouse.GetState();
            else {
                _previous = _current;
                _current = Mouse.GetState();
            }

            // Raise events to consumers
            if (_previous.LeftButton == ButtonState.Released &&
                _current.LeftButton == ButtonState.Pressed) {
                if (this.OnLMouseDown != null)
                    this.OnLMouseDown(this, new MouseEventArgs(new Vector2(_current.X, _current.Y)));
            }
            else if (_previous.LeftButton == ButtonState.Pressed &&
                _current.LeftButton == ButtonState.Released) {
                if (this.OnLMouseUp != null)
                    this.OnLMouseUp(this, new MouseEventArgs(new Vector2(_current.X, _current.Y)));
            }
        }
    }

    class MouseEventArgs : EventArgs {
        Vector2 _position;
        public Vector2 Position {
            get {
                return _position;
            }

        }

        public MouseEventArgs(Vector2 pos) {
            this._position = pos;
        }
    }
}
