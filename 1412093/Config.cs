﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace _1412093 {
    class Config : IDisposable
    {
        private static Config _instance = null;

        public static Config Instance
        {
            get { return _instance ?? (_instance = new Config()); }
        }

        private Config() { }

        public static JObject jsonCfg = null;

        public void Load()
        {
            string path = $"{Constants.APP_PATH}\\{Constants.config}";
            var jsonText = File.ReadAllText(Uri.UnescapeDataString(path));
            jsonCfg = JObject.Parse(jsonText);
            // jsonGeneralCfg["game_title"].ToString();
        }

        public void Save()
        {
            string path = $"{Constants.APP_PATH}\\{Constants.config}";
            File.WriteAllText(Uri.UnescapeDataString(path), jsonCfg.ToString());
        }

        public String[] parseNPCTextures(string npcName)
        {
            return (jsonCfg[npcName] as JArray).Select(v => v.ToString()).ToArray();
        }

        public void Dispose()
        {
            jsonCfg = null;
            GC.Collect();
        }
    }
}
